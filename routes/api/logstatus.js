var mongoose = require('mongoose');
var router = require('express').Router();
var moment = require("moment")
var auth = require('../auth');
var LogStatuses = mongoose.model('LogStatuses');

router.post("/statuspump/get", function(req, res, next){
  var qry = {}
  if(typeof req.body.station_id !== 'undefined'){
    qry.station_id = req.body.station_id
  }
  Promise.all([
    LogStatuses.find(qry).sort({_id: -1})
  ]).then(function(result){
      if (result[0].length > 0){
        var newResult = result[0]
        res.json(newResult)
      }else {
        res.json({
          msg: 0
        })
      }
  }).catch(next);
})

router.post("/statuspump", function(req, res, next){
  if(typeof req.body.station_id !== 'undefined' && typeof req.body.type !== 'undefined'){
    // var a = moment([2007, 0, 30, 00, 00, 00, 00]);
    // var b = moment([2008, 0, 29, 00, 00, 00, 00]);
    // var milliseconds = a.diff(b, 'milliseconds')
    // var seconds = a.diff(b, 'seconds')
    // return res.json({result: {
    //   milliseconds: milliseconds,
    //   seconds: seconds
    // }});
    var type = req.body.type
    var station_id = req.body.station_id 
    var timestamp_g = req.body.timestamp_g
    if (type === 'START'){ // type "START"
      // save
      // find last
      LogStatuses.findOne({
        station_id: station_id,
        type: "STOP",
        start_time: null
      }).sort({_id: -1}).limit(1).then(function(LogStatusesResult){
        if (LogStatusesResult === null){ // do save "start"
          var LogStatuses_new = new LogStatuses();
          LogStatuses_new.period_ms = null
          LogStatuses_new.station_id = station_id
          LogStatuses_new.type = type
          LogStatuses_new.start_time = timestamp_g
          LogStatuses_new.stop_time = null
          LogStatuses_new.save().then(function(result){
            return res.json({result: result});
          }).catch(next);
        }else { // do update time "stop" and save "start"
          LogStatusesResult.start_time = timestamp_g
          var a = moment(timestamp_g);
          var b = moment(LogStatusesResult.stop_time);
          var milliseconds = a.diff(b, 'milliseconds')
          LogStatusesResult.period_ms = milliseconds
          return LogStatusesResult.save().then(function(ResultUpdateStop){
            var LogStatuses_new = new LogStatuses();
            LogStatuses_new.period_ms = null
            LogStatuses_new.station_id = station_id
            LogStatuses_new.type = type
            LogStatuses_new.start_time = timestamp_g
            LogStatuses_new.stop_time = null
            LogStatuses_new.save().then(function(ResultSaveStart){
              return res.json({result: ResultSaveStart});
            }).catch(next);
          });
        }
      });
    }else if (type === "STOP") { // type "STOP"
      // update
      LogStatuses.findOne({
        station_id: station_id,
        type: "START",
        stop_time: null
      }).sort({_id: -1}).limit(1).then(function(LogStatusesResult){
        if (LogStatusesResult === null){ // do save "STOP"
          var LogStatuses_new = new LogStatuses();
          LogStatuses_new.period_ms = null
          LogStatuses_new.station_id = station_id
          LogStatuses_new.type = type // STOP 
          LogStatuses_new.start_time = null
          LogStatuses_new.stop_time = timestamp_g
          LogStatuses_new.save().then(function(result){
            return res.json({result: result});
          }).catch(next);
        }else { // do update time "start" and save "stop"
          LogStatusesResult.stop_time = timestamp_g
          var a = moment(timestamp_g);
          var b = moment(LogStatusesResult.start_time);
          var milliseconds = a.diff(b, 'milliseconds')
          LogStatusesResult.period_ms = milliseconds
          return LogStatusesResult.save().then(function(ResultUpdateStart){
            var LogStatuses_new = new LogStatuses();
            LogStatuses_new.period_ms = null
            LogStatuses_new.station_id = station_id
            LogStatuses_new.type = type
            LogStatuses_new.start_time = null
            LogStatuses_new.stop_time = timestamp_g
            LogStatuses_new.save().then(function(ResultSaveStop){
              return res.json({result: ResultSaveStop});
            }).catch(next);
          });
        }
      }).catch(next);
    }
  }else{
    res.json({
      error: 'station_id = undefined and type = undefined',
      status: false
    })
  }
  // client use function moment : moment().add(1154282, 'seconds');
  // var a = moment([2007, 0, 29]);
  // var b = moment([2007, 0, 28]);
  // different time : a.diff(b, 'seconds')
  // years, months, weeks, days, hours, minutes, and seconds. ,, milliseconds
})

module.exports = router;

/**
 * ปัญหาเมื่อ ไฟดับ ล๊อกเกอร์ออนไลน์
 */

// {
//   "station_id": "LKU-TEST",
//   "type": "START",
//   "timestamp_g": "2020-10-30T05:08:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "STOP",
//   "timestamp_g": "2020-10-30T05:18:00.000+00:00"
// }


// {
//   "station_id": "LKU-TEST",
//   "type": "START",
//   "timestamp_g": "2020-10-30T05:28:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "STOP",
//   "timestamp_g": "2020-10-30T05:38:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "START",
//   "timestamp_g": "2020-10-30T05:48:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "STOP",
//   "timestamp_g": "2020-10-30T05:58:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "START",
//   "timestamp_g": "2020-10-30T06:08:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "STOP",
//   "timestamp_g": "2020-10-30T06:18:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "START",
//   "timestamp_g": "2020-10-30T06:28:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "STOP",
//   "timestamp_g": "2020-10-30T06:58:00.000+00:00"
// }

// {
//   "station_id": "LKU-TEST",
//   "type": "START",
//   "timestamp_g": "2020-10-30T07:21:00.000+00:00"
// }

