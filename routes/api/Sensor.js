var mongoose = require('mongoose');
var router = require('express').Router();
var Sensor = mongoose.model('Sensor');
var Raw = mongoose.model('Raw');
var moment = require("moment")
var auth = require('../auth');
var point = require('./point.json')
var axios = require('axios');
var Speed = mongoose.model("Speed")

router.post("/PointChart", function(req, res, next){
  return res.json(point);
})

router.post("/current", function(req, res, next){
  var qry = {}
  if(typeof req.body.station_id !== 'undefined'){
    qry.station_id = req.body.station_id
  }
  Raw.find(qry).then(function (result){
    return res.json(result);
  })
})

router.get("/sensors/list", function(req, res, next){
  Sensor.find({}).then(function (result){
    var device = result.map((item, index) => {
      return item.station_id
    })
    return res.json({
      device: device,
      device_length: device.length,
      type: ["PO","TT","FC"],
      type_length: 3
    });
  })
})

router.post('/dynadata', function(req, res, next){
  var qry = {}
  Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
  }
  if(
    typeof req.body.WellName !== 'undefined' &&
    typeof req.body.year !== 'undefined' && 
    typeof req.body.month !== 'undefined' && 
    typeof req.body.day !== 'undefined' && 
    typeof req.body.offset !== 'undefined'
  ){
    var start = moment()
    // Date
    start.set('year', req.body.year)
    start.set('month', req.body.month - 1)
    start.set('date', req.body.day)
    // Time
    // start.set('hour', 0)
    // start.set('minute', 0)
    // start.set('second', 0)
    // start.set('millisecond', 0)
    start.add(req.body.offset, 'days');
    var end = moment()
    end.set('year', req.body.year)
    end.set('month', req.body.month - 1)
    end.set('date', req.body.day)
    end.add(-req.body.offset, 'days');
    qry.station_id = req.body.WellName,
    qry.DateTime = {}
    start = start.format("YYYY-MM-DD") + "T23:59:59.999+00:00"
    end = end.format("YYYY-MM-DD") + "T00:00:00.000+00:00"
    qry.DateTime.$lte = new Date(start)
    qry.DateTime.$gte = new Date(end)
  }
  Raw.find(qry).sort({DateTime: -1}).then(function(sensor){
    return res.json({
      data: sensor,
      row: sensor.length,
      qry: qry
    });
  }).catch(next);
})

router.post('/sensors/findone', function(req, res, next){
  var qry
  qry = req.body.station_id
  Sensor.findOne({
    station_id: qry,
  }).then(function(sensor){
    return sensor.save().then(function(result){
      return res.json(result);
    });
  }).catch(next);
})

router.post('/offline', auth.required, function(req, res, next){
  var raw = new Raw()
  raw.station_id = req.body.station_id
  raw.Loads = req.body.Loads
  raw.Positions = req.body.Positions
  raw.DateTime = req.body.timestamp_g
  raw.PumpCardLoads = []
  raw.PumpCardPositions = []
  raw.TT = req.body.TT
  raw.PT = req.body.PT
  raw.save().then(function(result){
    // send to api [ pump gas ] for update [PumpCardLoads, PumpCardPositions]
    axios.post('http://127.0.0.1:8888/pumpcard', {
      Collcetions_raws: {
        Loads: req.body.Loads,
        Positions: req.body.Positions,
        speed: req.body.speed,
        station_id: req.body.station_id,
        id : {
          raws_id: result._id
        }
      }
    })
    .then(data => {
      // save speeds
      var Speed_new = new Speed();
      if(typeof req.body.speed !== 'undefined'){
        Speed_new.station_id = req.body.station_id
        Speed_new.timestamp_s = new Date()
        Speed_new.timestamp_g = req.body.timestamp_g
        Speed_new.speed = req.body.speed
        Speed_new.save().then(function(resultSpeed){
          return res.json({
            resultSpeed: resultSpeed,
            DataPython: data.data,
            status: true
          });
        }).catch(next);
      }else{
        return res.json({
          DataPython: data.data,
          status: true
        });
      }
    })
    .catch(error => res.json({
      error: error,
      status: false
    }))
    // return res.json({result: result});
  }).catch(next);
});

router.post('/sensors', auth.required, function(req, res, next){
  // console.log(new Date(req.payload.exp * 1000)) // หมดอายุ
  // console.log(new Date(req.payload.iat * 1000)) // สร้างเมื่อ
  // console.log((req.payload.exp - req.payload.iat) < 0)
  // console.log(req.payload)
  if ((req.payload.exp - req.payload.iat) > 0){
    if (req.payload.type === "device"){
      var qry
      qry = req.body.station_id
      Sensor.findOne({
        station_id: qry
      }).then(function(sensor){
        if(sensor === null){
          return res.json(
            {
              error: 'ไม่พบอุปกรณ์',
              msg: 'กรุณาเพิ่มอุปกรณ์ : ' + qry
            }
          );
        }
        var sensor = sensor
        if(typeof req.body.Loads !== 'undefined'){
          sensor.Loads = req.body.Loads
        }
        if(typeof req.body.Positions !== 'undefined'){
          sensor.Positions = req.body.Positions
        }
        if(typeof req.body.PT !== 'undefined'){
          sensor.PT = req.body.PT
        }
        if(typeof req.body.TT !== 'undefined'){
          sensor.TT = req.body.TT
        }
        if(typeof req.body.DateTime !== 'undefined'){
          sensor.DateTime = req.body.DateTime
        }
        if(typeof req.body.timestamp_g !== 'undefined'){
          sensor.timestamp_g = req.body.timestamp_g
        }
        if(typeof req.body.rtu_status !== 'undefined'){
          sensor.rtu_status = req.body.rtu_status
        }
        if(typeof req.body.speed !== 'undefined'){
          sensor.speed = req.body.speed
        }
        if(typeof req.body.Parameter !== 'undefined'){
          sensor.Parameter = req.body.Parameter
        }
        if(typeof req.body.PumpCardPositions !== 'undefined'){
          sensor.PumpCardPositions = req.body.PumpCardPositions
        }
        if(typeof req.body.PumpCardLoads !== 'undefined'){
          sensor.PumpCardLoads = req.body.PumpCardLoads
        }
        if (typeof req.body.pump_status !== 'undefined'){
          sensor.pump_status = req.body.pump_status
        }
        if (typeof req.body.rod_config !== 'undefined'){
          sensor.rod_config = req.body.rod_config
        }
        sensor.timestamp_s = new Date()
        return sensor.save().then(function(result_sensor){
          if (req.body.Loads === undefined && req.body.Positions === undefined){
            // UPDATE PARAMETER ONLY
            return res.json({result: result_sensor, status: true});
          }else {
            var raw = new Raw()
            raw.station_id = result_sensor.station_id
            raw.Loads = result_sensor.Loads
            raw.Positions = result_sensor.Positions
            raw.DateTime = result_sensor.timestamp_g
            raw.Speed = result_sensor.speed
            raw.PumpCardLoads = []
            raw.PumpCardPositions = []
            raw.TT = result_sensor.TT
            raw.PT = result_sensor.PT
            raw.Parameter = result_sensor.Parameter
            raw.save().then(async function(result_raws){
              // send to api [ pump gas ] for update [PumpCardLoads, PumpCardPositions]
              axios.post('http://127.0.0.1:8888/pumpcard', {
                Collcetions_raws: {
                  Loads: result_raws.Loads,
                  Positions: result_raws.Positions,
                  speed: result_sensor.speed,
                  station_id: result_raws.station_id,
                  id : {
                    sensors_id: result_sensor._id,
                    raws_id: result_raws._id
                  }
                }
              })
              .then(data => {
                // save speeds
                var Speed_new = new Speed();
                if(typeof req.body.speed !== 'undefined'){
                  Speed_new.station_id = req.body.station_id
                  Speed_new.timestamp_s = new Date()
                  Speed_new.timestamp_g = req.body.timestamp_g
                  Speed_new.speed = req.body.speed
                  Speed_new.save().then(function(resultSpeed){
                    return res.json({
                      resultSpeed: resultSpeed,
                      DataPython: data.data,
                      status: true
                    });
                  }).catch(next);
                }else{
                  return res.json({
                    DataPython: data.data,
                    status: true
                  });
                }
              })
              .catch(error => res.json({
                error: error,
                status: false
              }))
              // old api
              // return res.json({result: result, status: true});
            }).catch(next);
          }
        });
      }).catch(next);
    }else {
      res.json({
        msg: "กรุณาใช้ device login",
        status: false
      })
    } 
  }else {
    res.json({
      msg: "Token หมดอายุ",
      status: false
    })
  }
});


router.delete('/sensors', function(req, res, next){
  Sensor.findOneAndRemove({_id: req.query.id}).then(function(result){
    return res.json(((result === null) ? {msg: 0} : {msg: 1}));
  });
});
router.get('/sensors', function(req, res, next){
  Promise.all([
    Sensor.find({})
  ]).then(function(result){
      if (result[0].length > 0){
        var newResult = result[0].map((item, index) => {
          var temp = {}
          temp._id = item._id
          temp.timestamp_s = item.timestamp_s
          temp.timestamp_g = item.timestamp_g
          temp.TT = item.TT
          temp.PT = item.PT
          temp.speed = item.speed
          temp.rtu_status = item.rtu_status
          temp.station_id = item.station_id
          temp.Loads = item.Loads
          temp.Positions = item.Positions
          temp.Parameter = item.Parameter
          temp.Loads_min = Math.min.apply(null, item.Loads) 
          temp.Loads_max = Math.max.apply(null, item.Loads) 
          temp.Positions_min = Math.min.apply(null, item.Positions) 
          temp.Positions_max = Math.max.apply(null, item.Positions) 
          temp.PumpCardLoads = item.PumpCardLoads
          temp.PumpCardPositions = item.PumpCardPositions
          temp.pump_status = item.pump_status
          temp.rod_config = item.rod_config
          return temp
        })
        res.json(newResult)
      }else {
        res.json({
          msg: 0
        })
      }
  }).catch(next);
});
router.put('/sensors', function(req, res, next){
  var Sensor_new = new Sensor();
  if(typeof req.body.station_id !== 'undefined'){
    Sensor_new.station_id = req.body.station_id;
  }
  if(typeof req.body.Parameter !== 'undefined'){
    Sensor_new.Parameter = req.body.Parameter;
  }else{
    Sensor_new.Parameter = []
  }
  Sensor_new.PT = 0
  Sensor_new.TT = 0
  Sensor_new.Loads = []
  Sensor_new.Positions = []
  Sensor_new.speed = 0
  Sensor_new.timestamp_g = new Date()
  Sensor_new.timestamp_s = new Date()
  Sensor_new.PumpCardLoads = []
  Sensor_new.PumpCardPositions = []
  Sensor_new.pump_status = false
  Sensor_new.rtu_status = false
  Sensor_new.save().then(function(result){
    return res.json({result: result});
  }).catch(next);
});

module.exports = router;


// hi. please type command follow me.
// cd   Software\ test
// chmod -R 777 *
// ./app
